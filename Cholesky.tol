//////////////////////////////////////////////////////////////////////////////
// FILE   : Cholesky.tol
// PURPOSE: Cholesky decomposition
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Class @Cholesky
//////////////////////////////////////////////////////////////////////////////
{
  Text _.autodoc.description = 
  "Class for handling with Cholesky decomposition in an efficient and easier "
  "way. You can solve all type of linear systems and matrix products related "
  "to this decomposition and store in OIS to be used in other TOL sessions.\n"
  "Given a symmetyric positive decomposition there is just one lower "
  "triangular matrix L matching\n"
  "\n"
  "  S = L L' \n"
  "\n"
  "It's posible that we have obtained L from any source, so is not efficient "
  "to rebuild it.\n"
  "Sometimes, as solving linear regressions, we have a rectangular squared root "
  "X of inverse R of matrix S that we want to handle with. Then it's more "
  "efficient to decompose the inverse of S and after get the inverse of the "
  "factor:\n"
  "\n"
  "  S^-1 = X' X = R = K K' \n"
  "  S^-1 = (L L')^-1 = L'^-1 L^-1 \n"
  "  K' = L^-1 \n"
  "\n"
  "Note that in this case L is upper triangular instead of lower triangular, "
  "so S = L L' is not the Cholesky decomposition of S but the upper Cholesky "
  "decomposition. Normally itsn't an important question due to any square "
  "squared root is valid.\n"
  "You need to give just one of five related matrices S, L, X, R or K calling "
  "set.S, set.L, set.X, set.R or set.K respectively. Rest of them will be "
  "built when necessary.\n"
  "Internal methods are implemented using dense or sparse methods taking "
  "account the structure of the specified matrices. ";

  Text _.autodoc.member._.buildingMethod = "Building method must be one of "
  "these: \"S\", \"Lf\", \"X\", \"R\" or\"Kf\"";
  Text _.buildingMethod = "";

  Text _.autodoc.member._.isSparse = "Sparse matrices are treated in an "
  "special and faster way than dense ones using CHOLMOD by means of VMatrix";
  Real _.isSparse = ?;

  Text _.autodoc.member._.isGood = "True if decomposition and all related "
  "operations have worked fine.";
  Real _.isGood = False;

  Text _.autodoc.member._.n = "Size of related square matrices";
  Real _.n = ?;

  Text _.autodoc.member._.S = "Original symmetric positive definite matrix "
  "to be decomposed.";
  VMatrix _.S = Constant(0,0,?);  

  Text _.autodoc.member._.Lf = "Cholesky factor of original symmetric "
  "positive definite matrix \n"
  "\n"
  "  S = L L' \n"
  "\n"
  "In sparse case this is the internal CHOLMOD representation of L used to "
  "solve systems faster.\n"
  "If S is dense then _.Lf and _.Le are the same thing.";
  VMatrix _.Lf = Constant(0,0,?);  

  Text _.autodoc.member._.Le = "Cholesky factor of original symmetric "
  "positive definite matrix \n"
  "\n"
  "  S = L L' \n"
  "\n"
  "This is the explicit sparse or dense storement of L.\n"
  "In dense case then _.Lf and _.Le are the same thing.";
  VMatrix _.Le = Constant(0,0,?);  

  Text _.autodoc.member._.R = "Inverse of original symmetric positive "
  "definite matrix \n"
  "\n"
  "  R = S^-1 \n"
  "\n"
  "If we have the inverse of S, then it's more efficient to decompose "
  "the inverse of S and after inverse the factor if needed. If we need "
  "just to solve linear systems and get products of related functions "
  "then is not needed to get the inverse of factor.";
  VMatrix _.R = Constant(0,0,?);  

  Text _.autodoc.member._.X = "Sometimes, as solving linear regressions, "
  "we have the input matrix X, a regular rectangular matrix that is the "
  "squared root of R, the inverse of original symmetric positive definite "
  "matrix S: \n"
  "\n"
  "  X' X = R = S^-1 \n"
  "\n"
  "Then it's more efficient "
  "to decompose the inverse of S and after inverse the factor.";
  VMatrix _.X = Constant(0,0,?);  

  Text _.autodoc.member._.Kf =
  "Cholesky factor of inverse of original symmetric positive definite "
  "matrix \n"
  "\n"
  "  S^-1 = R = K K' \n"
  "\n"
  "Alternatively, it's the trasposed inverse of Cholesky factor of original "
  "symmetric positive definite matrix \n"
  "\n"
  "  S^-1 = (L L')^-1 = L'^-1 L^-1 \n"
  "  K' = L^-1 \n"
  "In sparse case this is the internal CHOLMOD representation of K used to "
  "solve systems faster.\n"
  "In dense case then _.Kf and _.Ke are the same thing.";
  VMatrix _.Kf = Constant(0,0,?);  

  Text _.autodoc.member._.Ke =
  "Cholesky factor of inverse of original symmetric positive definite "
  "matrix \n"
  "\n"
  "  S^-1 = R = K K' \n"
  "\n"
  "Alternatively, it's the trasposed inverse of Cholesky factor of original "
  "symmetric positive definite matrix \n"
  "\n"
  "  S^-1 = (L L')^-1 = L'^-1 L^-1 \n"
  "  K' = L^-1 \n"
  "This is the explicit sparse or dense storement of K.\n"
  "In dense case then _.Kf and _.Ke are the same thing.";
  VMatrix _.Ke = Constant(0,0,?);  

  Real showErrWar = True;
  
  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member._invalid = "Shows an error message called from a "
  "method for the specified reason.";
  Real _invalid(Text method, Text reason)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real _.isGood := False; 
    WriteLn("[@Cholesky::"+method+"] cannot be used.\n"+reason,"E");
    False
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member._checkSubtype = "";
  Real _checkSubtype(VMatrix M, Text member)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(!_.isGood, False, 
    {
      Real _.isGood := True;
      Text st = SubType(M);
      Text m = Sub(member,1,1);
      Case(
      And(Or(m=="L",m=="K"), st=="Cholmod.R.Factor"), 
      {
        _.isSparse := True
      },
      And(Or(m=="S",m=="R",m=="X"),st=="Cholmod.R.Sparse"), 
      {
        _.isSparse := True
      },
      st=="Blas.R.Dense",                                   
      {
        _.isSparse := False
      },
      1==1, 
      {
        Real _invalid("set."+member,"Invalid subtype of \n"
        "  VMatrix _."+member+": "<<st);
        _.isSparse := ?
      });
      If(!_.isGood,False,
      {
        _.n := VColumns(M);
        True
      })
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.set.Lf = "Sets the Cholesky factor L in the implicit "
  "internal format (_.Lf) and builds explicit one (_.Le).";
  Real set.Lf(VMatrix Lf) 
  ////////////////////////////////////////////////////////////////////////////
  {
    If(!_checkSubtype(Lf,"Lf"), False, {
      VMatrix _.Lf := Lf;
      Real Show(False,"WARNING",True);
      VMatrix _.Le := CholeskiSolve(_.Lf,Convert(_.Lf,"Cholmod.R.Sparse"),"P");
      Real Show(True,"WARNING",True);
      _.isGood
    })
  };  

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.set.S = "Sets the original symmetric positive definite "
  "matrix S to be decomposed.";
  Real set.S(VMatrix S) 
  ////////////////////////////////////////////////////////////////////////////
  {
    If(!_checkSubtype(S,"S"), False, {
      VMatrix _.S := (S+Tra(S))*0.5;
      VMatrix L = CholeskiFactor(_.S, "X", True,showErrWar);
      Text st = SubType(L);
      If(st=="Undefined",_.isGood:=False, set.Lf(L))
    })
  };  

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.set.Kf = "Sets the Cholesky factor K in the implicit "
  "internal format (_.Kf) and builds explicit one (_.Ke).";
  Real set.Kf(VMatrix Kf) 
  ////////////////////////////////////////////////////////////////////////////
  {
    If(!_checkSubtype(Kf,"Kf"), False, {
      VMatrix _.Kf := Kf;
      Real Show(False,"WARNING",True);
      VMatrix _.Ke :=  CholeskiSolve(_.Kf,Convert(_.Kf, "Cholmod.R.Sparse"),"P");
      Real Show(True,"WARNING",True);
      _.isGood
    })
  };  

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.set.R = "Sets R, the inverse symmetric positive "
  "definite matrix S to be decomposed.";
  Real set.R(VMatrix R) 
  ////////////////////////////////////////////////////////////////////////////
  {
    If(!_checkSubtype(R,"R"), False, {
      VMatrix _.R := (R+Tra(R))*0.5;
      VMatrix R = CholeskiFactor(_.R, "X", True,showErrWar);
      Text st = SubType(R);
      If(st=="Undefined",_.isGood:=False, set.Kf(R))
    })
  };  

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.set.X = "Sets X, the regular rectangular factor of "
  "R, the inverse symmetric positive definite matrix S to be decomposed.";
  Real set.X(VMatrix X) 
  ////////////////////////////////////////////////////////////////////////////
  {
    If(!_checkSubtype(X,"X"), False, {
      VMatrix _.X := X;
      VMatrix R = CholeskiFactor(_.X, "XtX", True,showErrWar);
      Text st = SubType(R);
      If(st=="Undefined",_.isGood:=False, set.Kf(R))
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.build = "";
  Real build(Text buildingMethod, VMatrix M) 
  ////////////////////////////////////////////////////////////////////////////
  {
    Real _.isGood := True;
    Text _.buildingMethod := buildingMethod;
    Real Case(
    buildingMethod=="S",  set.S (M),
    buildingMethod=="Lf", set.Lf(M),
    buildingMethod=="X",  set.X (M),
    buildingMethod=="R",  set.R (M),
    buildingMethod=="Kf", set.Kf(M),
    1==1,
    {
      _invalid("Build","Invalid argument buildingMethod must be one of "
      "these: \"S\", \"Lf\", \"X\", \"R\" or\"Kf\"")
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.Create = "";
  Static @Cholesky Create(Text buildingMethod, VMatrix M) 
  ////////////////////////////////////////////////////////////////////////////
  {
    @Cholesky new;
    Real new::build(buildingMethod, M);
    new
  };  

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.CreateMute = "";
  Static @Cholesky CreateMute(Text buildingMethod, VMatrix M) 
  ////////////////////////////////////////////////////////////////////////////
  {
    @Cholesky new =
    [[
      Real showErrWar = False
    ]];
    Real new::build(buildingMethod, M);
    new
  };  

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.L_prod_M = "Returns L * M";
  VMatrix L_prod_M(VMatrix M)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = VRows(M);
    Case(
    !_.isGood,
    {
      Real _invalid("L_prod_M","Not well defined instance. ");
      Constant(0,0,?)
    },
    n!=_.n,
    {
      Real _invalid("L_prod_M", "Invalid dimensions. VRows(M)="<<n+"!="<<_.n);
      Constant(0,0,?)
    },
    VRows(_.Le)==_.n, _.Le * M,
    VRows(_.Kf)==_.n, CholeskiSolve(_.Kf, M, "LtP") ,
    1==1,
    {
      Real _invalid("L_prod_M", "Corrupted instance: _.Le or _.Kf must be "
      "("<<_.n+"x"<<_.n+") ");
      Constant(0,0,?)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.M_prod_L = "Returns M * L";
  VMatrix M_prod_L(VMatrix M)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = VColumns(M);
    Case(
    !_.isGood,
    {
      Real _invalid("M_prod_L","Not well defined instance. ");
      Constant(0,0,?)
    },
    n!=_.n,
    {
      Real _invalid("M_prod_L", "Invalid dimensions. VColumns(M)="<<n+"!="<<_.n);
      Constant(0,0,?)
    },
    VRows(_.Le)==_.n, M * _.Le,
    VRows(_.Kf)==_.n, Tra(CholeskiSolve(_.Kf, Tra(M), "PtL")) ,
    1==1,
    {
      _invalid("M_prod_L", "Corrupted instance: _.Le or _.Kf must be "
      "("<<_.n+"x"<<_.n+") ");
      Constant(0,0,?)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.Lt_prod_M = "Returns L' * M";
  VMatrix Lt_prod_M(VMatrix M)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = VRows(M);
    Case(
    !_.isGood,
    {
      Real _invalid("Lt_prod_M","Not well defined instance. ");
      Constant(0,0,?)
    },
    n!=_.n,
    {
      Real _invalid("Lt_prod_M", "Invalid dimensions. VRows(M)="<<n+"!="<<_.n);
      Constant(0,0,?)
    },
    VRows(_.Le)==_.n, Tra(Tra(M)*_.Le),
    VRows(_.Kf)==_.n, CholeskiSolve(_.Kf, M, "PtL") ,
    1==1,
    {
      Real _invalid("Lt_prod_M", "Corrupted instance: _.Le or _.Kf must be "
      "("<<_.n+"x"<<_.n+") ");
      Constant(0,0,?)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.M_prod_Lt = "Returns M * L'";
  VMatrix M_prod_Lt(VMatrix M)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = VColumns(M);
    Case(
    !_.isGood,
    {
      Real _invalid("M_prod_Lt","Not well defined instance. ");
      Constant(0,0,?)
    },
    n!=_.n,
    {
      Real _invalid("M_prod_Lt", "Invalid dimensions. VColumns(M)="<<n+"!="<<_.n);
      Constant(0,0,?)
    },
    VRows(_.Le)==_.n, Tra(_.Le*Tra(M)),
    VRows(_.Kf)==_.n, Tra(CholeskiSolve(_.Kf, Tra(M), "LtP")) ,
    1==1,
    {
      _invalid("M_prod_Lt", "Corrupted instance: _.Le or _.Kf must be "
      "("<<_.n+"x"<<_.n+") ");
      Constant(0,0,?)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.K_prod_M = "Returns K * M";
  VMatrix K_prod_M(VMatrix M)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = VRows(M);
    Case(
    !_.isGood,
    {
      Real _invalid("K_prod_M","Not well defined instance. ");
      Constant(0,0,?)
    },
    n!=_.n,
    {
      Real _invalid("K_prod_M", "Invalid dimensions. VRows(M)="<<n+"!="<<_.n);
      Constant(0,0,?)
    },
    VRows(_.Ke)==_.n, _.Ke * M,
    VRows(_.Lf)==_.n, CholeskiSolve(_.Lf, M, "LtP"),
    1==1,         
    {
      Real _invalid("K_prod_M", "Corrupted instance: _.Ke or _.Lf must be "
      "("<<_.n+"x"<<_.n+") ");
      Constant(0,0,?)    
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.M_prod_K = "Returns M * K";
  VMatrix M_prod_K(VMatrix M)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = VColumns(M);
    Case(
    !_.isGood,
    {
      Real _invalid("M_prod_K","Not well defined instance. ");
      Constant(0,0,?)
    },
    n!=_.n,
    {
      Real _invalid("M_prod_K", "Invalid dimensions. VColumns(M)="<<n+"!="<<_.n);
      Constant(0,0,?)
    },
    VRows(_.Ke)==_.n, M * _.Ke;
    VRows(_.Lf)==_.n, Tra(CholeskiSolve(_.Lf, Tra(M), "PtL")),
    1==1,
    {
      Real _invalid("M_prod_K", "Corrupted instance: _.Le or _.Kf must be "
      "("<<_.n+"x"<<_.n+") ");
      Constant(0,0,?)
    })
  };


  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.Kt_prod_M = "Returns K' * M";
  VMatrix Kt_prod_M(VMatrix M)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = VRows(M);
    Case(
    !_.isGood,
    {
      Real _invalid("Kt_prod_M","Not well defined instance. ");
      Constant(0,0,?)
    },
    n!=_.n,
    {
      Real _invalid("Kt_prod_M", "Invalid dimensions. VRows(M)="<<n+"!="<<_.n);
      Constant(0,0,?)
    },
    VRows(_.Ke)==_.n, Tra(Tra(M)*_.Ke),
    VRows(_.Lf)==_.n, CholeskiSolve(_.Lf, M, "PtL") ,
    1==1,
    {
      Real _invalid("Kt_prod_M", "Corrupted instance: _.Ke or _.Lf must be "
      "("<<_.n+"x"<<_.n+") ");
      Constant(0,0,?)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.M_prod_Kt = "Returns M * K'";
  VMatrix M_prod_Kt(VMatrix M)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = VColumns(M);
    Case(
    !_.isGood,
    {
      Real _invalid("M_prod_Kt","Not well defined instance. ");
      Constant(0,0,?)
    },
    n!=_.n,
    {
      Real _invalid("M_prod_Kt", "Invalid dimensions. VColumns(M)="<<n+"!="<<_.n);
      Constant(0,0,?)
    },
    VRows(_.Ke)==_.n, Tra(_.Ke*Tra(M)),
    VRows(_.Lf)==_.n, Tra(CholeskiSolve(_.Lf, Tra(M), "LtP")) ,
    1==1,
    {
      _invalid("M_prod_Kt", "Corrupted instance: _.Ke or _.Lf must be "
      "("<<_.n+"x"<<_.n+") ");
      Constant(0,0,?)
    })
  };


  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.S_prod_M = "Returns S * M";
  VMatrix S_prod_M(VMatrix M)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = VRows(M);
    Case(
    !_.isGood,
    {
      Real _invalid("S_prod_M", "Not well defined instance. ");
      Constant(0,0,?)
    },
    n!=_.n,
    {
      Real _invalid("S_prod_M", "Invalid dimensions. VRows(M)="<<n+"!="<<_.n);
      Constant(0,0,?)
    },
    VRows(_.S)==_.n, _.S * M,
    1==1,
    {
      L_prod_M(Lt_prod_M(M))
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.R_prod_M = "Returns R * M";
  VMatrix R_prod_M(VMatrix M)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = VRows(M);
    Case(
    !_.isGood,
    {
      Real _invalid("R_prod_M", "Not well defined instance. ");
      Constant(0,0,?)
    },
    n!=_.n,
    {
      Real _invalid("R_prod_M", "Invalid dimensions. VRows(M)="<<n+"!="<<_.n);
      Constant(0,0,?)
    },
    VRows(_.R)==_.n, _.R * M,
    1==1,
    {
      K_prod_M(Kt_prod_M(M))
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.M_prod_S = "Returns S * M";
  VMatrix M_prod_S(VMatrix M)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = VColumns(M);
    Case(
    !_.isGood,
    {
      Real _invalid("M_prod_S", "Not well defined instance. ");
      Constant(0,0,?)
    },
    n!=_.n,
    {
      Real _invalid("M_prod_S", "Invalid dimensions. VColumns(M)="<<n+"!="<<_.n);
      Constant(0,0,?)
    },
    VRows(_.S)==_.n, M*_.S,
    1==1,
    {
      M_prod_Lt(M_prod_L(M))
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.M_prod_R = "Returns M * R";
  VMatrix M_prod_R(VMatrix M)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = VColumns(M);
    Case(
    !_.isGood,
    {
      Real _invalid("M_prod_R", "Not well defined instance. ");
      Constant(0,0,?)
    },
    n!=_.n,
    {
      Real _invalid("M_prod_R", "Invalid dimensions. VColumns(M)="<<n+"!="<<_.n);
      Constant(0,0,?)
    },
    VRows(_.R)==_.n, M*_.R,
    1==1,
    {
      M_prod_Kt(M_prod_K(M))
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.Mt_prod_S_prod_M = "Returns M' * S * M";
  VMatrix Mt_prod_S_prod_M(VMatrix M)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = VRows(M);
    Case(
    !_.isGood,
    {
      Real _invalid("Mt_prod_S_prod_M", "Not well defined instance. ");
      Constant(0,0,?)
    },
    n!=_.n,
    {
      Real _invalid("Mt_prod_S_prod_M", "Invalid dimensions. VRows(M)="<<n+"!="<<_.n);
      Constant(0,0,?)
    },
    VRows(_.S)==_.n, Tra(M) * _.S * M,
    1==1,
    {
      MtMSqr(Lt_prod_M(M))
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.Mt_prod_R_prod_M = "Returns M' * R * M";
  VMatrix Mt_prod_R_prod_M(VMatrix M)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = VRows(M);
    Case(
    !_.isGood,
    {
      Real _invalid("Mt_prod_R_prod_M", "Not well defined instance. ");
      Constant(0,0,?)
    },
    n!=_.n,
    {
      Real _invalid("Mt_prod_R_prod_M", "Invalid dimensions. VRows(M)="<<n+"!="<<_.n);
      Constant(0,0,?)
    },
    VRows(_.R)==_.n, Tra(M) * _.R * M,
    1==1,
    {
      MtMSqr(Kt_prod_M(M))
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.log_det_S = "Returns logarithm of determinant of S";
  Real log_det_S(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Case(
    !_.isGood,
    {
      Real _invalid("log_det_S", "Not well defined instance. ");
      ?
    },
    VRows(_.Le)==_.n,  2*VMatSum(Log(SubDiag(_.Le))),
    VRows(_.Ke)==_.n, -2*VMatSum(Log(SubDiag(_.Ke))),
    1==1,
    {
      Real _invalid("log_det_S", "Corrupted instance: _.Ke or _.Lf must be "
      "("<<_.n+"x"<<_.n+") ");
      ?
    })
  }


};
